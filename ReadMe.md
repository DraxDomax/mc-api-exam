# Exam project - API test

Hello :) Please accept my solution to the API test given.

This is available on gitlab: https://gitlab.com/DraxDomax/mc-api-exam
The repo is public but no names of anything are mentioned

## To Run
0. Prerequisites: Java 11+, Maven, internet connection
1. There is one test, you can elect to run "just" that one with: `mvn test -Dcucumber.options="--tags @PetsByStatus"`
2. Project comes with default usage of WireMock. If you want to use the actual online resource, please edit the `flags` file by changing the `useMock` property to False
3. You can find reports in `target/cucumber-reports.html` 

## Notes
1. I was wondering about the logic with the optional WireMock - does it replace the
fallback to JSON file reading. I decided to keep both, to show I can read files too ;)
2. Assumed some limit on the scope of this task. Of course, many things can be added or made better but, at some point, it can become superfluous to the goal of this endeavor. Should you require any changes, please let me know

## Todo:
1. Improve deprecated `execute()` call
2. Add CI to gitlab project
3. Add more tests (implied schema validation, unit tests for client)