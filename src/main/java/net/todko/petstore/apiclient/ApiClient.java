package net.todko.petstore.apiclient;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.todko.petstore.dto.PetObject;
import net.todko.petstore.dto.PetStatusObject;

public class ApiClient {

    private static final Logger logger = LogManager.getLogger(ApiClient.class);
    private static final String BASE_URL = "/v2/pet/findByStatus?status=";
    private final String host;

    public ApiClient(String host) {
        this.host = host;
    }

    public List<PetObject> getPetsByStatus(PetStatusObject petStatus) {

        ObjectMapper objectMapper = new ObjectMapper();
        ClassicHttpResponse response;

        String pets;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

            HttpGet request = new HttpGet(host + BASE_URL + petStatus.toString().toLowerCase());

            response = null;
            try {
                logger.info("Trying to execute http request with url " + request.getRequestUri());
                // TODO: Find out the request dispatch currently in fashion (`.execute()` is deprecated):
                response = httpClient.execute(request);
            } catch (IOException e) {
                logger.warn("Failed to execute the request");
                e.printStackTrace();
            }

            if (response != null && response.getCode() == 200) {
                try {
                    logger.info("Parsing the response");
                    pets = EntityUtils.toString(response.getEntity());
                    logger.debug("Payload:"+pets);
                } catch (IOException | ParseException e) {
                    logger.warn("An error happened during response parsing");
                    e.printStackTrace();
                    pets = readPetsJson();
                }
            } else {
                pets = readPetsJson();
            }

        } catch (IOException e) {
            pets = readPetsJson();
            e.printStackTrace();
        }

        try {
            logger.info("Transforming string to the list");
            return objectMapper.readValue(pets, new TypeReference<ArrayList<PetObject>> () {});
        } catch (JsonProcessingException e) {
            logger.error("Can't parse the string");
            e.printStackTrace();
            return null;
        }
    }

    private static String readPetsJson() {
        try {
            logger.info("Reading the default json file instead");
            return Files.readString(Path.of("pets.json"));
        } catch (IOException e) {
            logger.error("No default json file. Can't build the list");
            e.printStackTrace();
            return null;
        }
    }
}
