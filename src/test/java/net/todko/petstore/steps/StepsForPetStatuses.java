package net.todko.petstore.steps;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.matching.EqualToPattern;
import io.cucumber.java.After;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.todko.petstore.apiclient.ApiClient;
import net.todko.petstore.dto.PetObject;
import net.todko.petstore.dto.PetStatusObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;


import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Properties;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class StepsForPetStatuses {

    private List<PetObject> pets;

    private long matchingCount;

    private static final Logger logger = LogManager.getLogger(StepsForPetStatuses.class);

    private ApiClient client;

    WireMockServer wireMockServer;

    @ParameterType(value = "true|True|TRUE|false|False|FALSE")
    public Boolean booleanValue(String value) {
        return Boolean.valueOf(value);
    }

    private void configureClient() {
        /**
        Sets up a state retrieval client which points to an online resource or to a local JSON
         **/

        // Read flags to determine whether WireMock is required or the online stuff (from `flags` file in project root)
        Properties props = new Properties();
        try {
            props.load(new FileInputStream("flags"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        boolean useMock = Boolean.parseBoolean(props.getProperty("useMock"));

        logger.info("Use mock is"+useMock);
        if (useMock) {
            logger.info("Configuring WireMock");
            wireMockServer = new WireMockServer(wireMockConfig().port(8089));
            String response = null;
            try {
                response = Files.readString(Path.of("pets.json"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            wireMockServer.stubFor(get("/v2/pet/findByStatus").withQueryParam("status", new EqualToPattern("available"))
                    .willReturn(ok().withHeader("Content-Type", "application/json").withBody(response)));
            logger.info("Starting mock");
            wireMockServer.start();
            client = new ApiClient("http://localhost:8089");
        } else {
            client = new ApiClient("https://petstore.swagger.io");
        }
    }

    @Given("list of pets with available status")
    public void getListPfPets() {
        configureClient();
        logger.info("invoking the method getPetsByStatus");
        pets = client.getPetsByStatus(PetStatusObject.AVAILABLE);
    }

    @When("we calculate pets with name {word}")
    public void filterByName(String name) {
        logger.info("Filtering data by expected name");
        matchingCount = pets.stream().filter(item -> item.getName() != null && item.getName().equals(name)).count();
        logger.info("The matching records count is "+matchingCount);
    }

    @Then("the count should be {int}")
    public void checkCount(int expectedCount) {
        logger.info("Check it matches expected count");
        Assertions.assertEquals(expectedCount, matchingCount, "Counts do not match");
    }

    @After
    public void stopMock() {
        if (wireMockServer != null) {
            wireMockServer.stop();
        }
    }
}
